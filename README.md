# Project2 - pageserver

* Revised proj1 into flask frame.
* Show correctly trivia.css or trivia.html.
* It has 2 errorhandler which is 403 and 404, alone with 2 html files.

# Information

## Liwei Fan ##
## liweif@uoregon.edu

# Grading Rubric
* If your code works as expected: 100 points.

* For every wrong functionality (i.e., (a), (b), and (c) from project 1), 20 points will be docked off. 

* If none of the functionalities work, 40 points will be given assuming 
    * the credentials.ini is submitted with the correct URL of your repo, 
    * the Dockerfile builds without any errors, and 
    * if the two html files (404.html and 403.html) are created in the appropriate location. 
    
* If the Dockerfile doesn't build or is missing, 20 points will be docked off.

* If the two html files are missing, 20 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.

# Who do I talk to? ###

* Maintained by Ram Durairajan, Steven Walton.
* Use our Piazza group for questions. Make them public unless you have a good reason to make them private, so that everyone benefits from answers and discussion. 