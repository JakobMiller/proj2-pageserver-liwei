from flask import Flask, render_template, request

app = Flask(__name__, template_folder='./pages', static_folder='./pages')

@app.route("/")
def hello():
    return 'Hello World!'

@app.route("/pages/<name>")
def page1(name):
    try:
        return render_template(name)
    except:
        return internal_error(404)


@app.errorhandler(403)
def not_allowed(error):
    return render_template("403.html"), 403

@app.errorhandler(404)
def internal_error(error):
    if '//' in request.path or '..' in request.path or '~' in request.path:
        return not_allowed(403)
    else:
        return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
